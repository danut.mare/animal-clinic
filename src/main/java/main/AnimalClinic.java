package main;

import dao.ConsultDao;
import dao.PetDao;
import dao.VetDao;
import enity.*;
import service.InputOutputService;
import service.UtilityService;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Scanner;
import java.util.stream.Collectors;

public class AnimalClinic {
    private Scanner scanner = new Scanner(System.in);
    // am creat o instanta a lui IOService  pentru a avea acces la taote metodele din clasa
    private UtilityService utilityService = new UtilityService();
    private InputOutputService inputOutputService = new InputOutputService();
    private VetDao vetDao = new VetDao();
    private PetDao petDao = new PetDao();
    private ConsultDao consultDao = new ConsultDao();


    //aici se afla scheletul aplicatiei
    public void start() {
        while(true) { // repetam acesti pasi la "infinit" print acest WHILE
            inputOutputService.displayMenu(); // afisam meniul principal
            String userInput = inputOutputService.getUserInput(); // luam inputul de la user
            if(isExitCodition(userInput)){ // daca input-ul de la user este "X", ne asiguram ca aplicatia se va opri
                break;
            }
            process(userInput); // procesam inputul de la user
        }
    }

    private boolean isExitCodition(String userInput) {
        return userInput.equalsIgnoreCase("x");
    }

    private void process(String userInput) {
        switch(userInput){ // facem test-case-uri pentru a executa comanda pe care o doreste user-ul
            case "1":{
                addVet();
                break;
            }
            case "2":{
                addPet();
                break;
            }
            case "3":{
                addConsult();
                break;
            }
            case "4":{
                addWorkInterval();
                break;
            }
        }
    }

    private void addWorkInterval() {
        inputOutputService.witchVetQuestion();
        List<Vet> allVets = vetDao.findAll();
        Integer vetIndx;

        inputOutputService.displayVetsAppointmentMessage(allVets);
        while (true) {
            String vetIndexAsString = inputOutputService.readYourAnswer();
            Optional<Integer> vetIndex = parseVetIndex(vetIndexAsString);

            if (vetIndex.isPresent()) {
                vetIndx = vetIndex.get();
                // facem aceasta conditie pentru a ne asigura ca aplicatia nu se va opri cand input-ul
                // o sa fie de exemplu (-1, -2 sau vetIndex +1, +2 etc)
                if (isIndexValid(allVets, vetIndx)) {
                    break;
                }
            }
            inputOutputService.displayErrorMessage("Input");
        }
        // aici ne asiguram ca luam vet-ul de pe pozitia care trebuie
        // deoarece daca nu am face vetIndx -1, noi putem selecta vet-ul cu indexul 10
        // dar el se afla pe pozitia 9 iar atunci o sa avem un IndexOutOfBound
        Vet vet = allVets.get(vetIndx - 1);

        DayOfWeek[] days = DayOfWeek.values();
        Optional<LocalTime> optionalStartHour;
        Optional<LocalTime> optionalEndHours;
            // prin aceasta conditie dorim sa se sara peste zile de SATURDAY si SUNDAY
            for (DayOfWeek day : days) {
                if (day == DayOfWeek.SATURDAY || day == DayOfWeek.SUNDAY) {
                    continue;
                }
                optionalStartHour = getStartHour(day);
                optionalEndHours = getEndHour(day);
                Optional<WorkDaySchedule> optionalWorkDaySchedule = vet.getScheduleFor(day);

                if (optionalWorkDaySchedule.isPresent()) {
                    if (optionalWorkDaySchedule.isPresent()) {
                        WorkDaySchedule workDaySchedule = optionalWorkDaySchedule.get();
                        workDaySchedule.setStartTime(optionalStartHour.get());
                        workDaySchedule.setEndTime(optionalEndHours.get());
                    }
                } else {
                    WorkDaySchedule workDaySchedule = new WorkDaySchedule(optionalStartHour.get(), optionalEndHours.get(), day, vet);
                    vet.addSchedule(workDaySchedule);
                }
            }
            vetDao.merge(vet);
        }



    private Optional<LocalTime> getStartHour(DayOfWeek day) {
        Optional<LocalTime> optionalStartHour;
        while (true) {
            String startWorkingHours = inputOutputService.getStartingHour(day);
            optionalStartHour = startTime(startWorkingHours);
            if (optionalStartHour.isPresent()) {
                break;
            } else {
                inputOutputService.displayErrorMessage("Input");
            }
        }
        return optionalStartHour;
    }


    private Optional<LocalTime> getEndHour(DayOfWeek day) {
        Optional<LocalTime> optionalEndHours;
        while(true) {
            String endWorkingHours = inputOutputService.getEndHours(day);
            optionalEndHours = endTime(endWorkingHours);
            if(optionalEndHours.isPresent()){
                break;
            }else{
                inputOutputService.displayErrorMessage("Input");
            }
        }
        return optionalEndHours;
    }



    private Optional<LocalTime> startTime(String startWorkingHours){
        try{
            DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("H:mm");
            LocalTime startHour = LocalTime.parse(startWorkingHours, dateTimeFormatter);
            return Optional.of(startHour);
        }catch (DateTimeParseException e){
            return Optional.empty();
        }
    }

    private Optional<LocalTime> endTime(String endWorkingHours){
        try{
            DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("H:mm");
            LocalTime endHours = LocalTime.parse(endWorkingHours, dateTimeFormatter);
            return Optional.of(endHours);
        }catch (DateTimeParseException e){
            return Optional.empty();
        }
    }

    private void addConsult() {
        Vet vet = getVet();

        Optional<WorkDaySchedule> scheduleFor;
        LocalDate localDateForConsult;
        while(true) {
            Optional<LocalDate> dateForConsult = getConsultDate();
            if(dateForConsult.isPresent()){
                // in aceasta variabila de tipul LocalDate stocam input pe care l-am primit de la user
                localDateForConsult = dateForConsult.get();
                // iar mai apoi pe baza inputului primit verificam ziua care coreleaza cu data primita
                scheduleFor = getWorkDaySchedule(vet, localDateForConsult);
                if (scheduleFor.isPresent()) {
                    break;
                }
            }
            inputOutputService.displayErrorMessage("Input");
        }
            WorkDaySchedule workDaySchedule = scheduleFor.get();
            System.out.println("Please enter a time between " + workDaySchedule.getStartTime() + " and " + workDaySchedule.getEndTime());
            LocalDate finalLocalDateForConsult = localDateForConsult;
            List<LocalTime> scheduledConsulationHours = getScheduledConsultationHours(vet, finalLocalDateForConsult);

            List<LocalTime> availableForScheduling = getAvailableForSchedulingHours(workDaySchedule, scheduledConsulationHours);
        
            // aici verificam daca lista cu consultatii este goala, daca este goala si vet-ul este full in data selectata de user
            // atunci ii afisam mesajul "All booked" si iesim din metoda fara ai da a doua sansa de a selecta alta data
            if(availableForScheduling.isEmpty()){
                inputOutputService.allBookedMessage();
                return;
            }
            Optional<LocalTime> optionalConsultTime = getConsultTime(availableForScheduling);
            String ownerName = inputOutputService.read("your name");
            String petName = inputOutputService.read("your pet's name");
            Optional<Pet> optionalPet = petDao.findByOwnerNameAndPetName(ownerName, petName);

            if(!optionalPet.isPresent()){
                inputOutputService.noPetsError();
                return;
            }
            Pet pet = optionalPet.get();
            LocalDateTime datetTime = LocalDateTime.of(localDateForConsult, optionalConsultTime.get());
            String description = inputOutputService.readDescription();
            inputOutputService.consultScheduled();
            Consult consult = new Consult(vet, pet, datetTime, description);
            consultDao.save(consult);
    }

    private Optional<LocalTime> getConsultTime(List<LocalTime> availableForScheduling) {
        Optional<LocalTime> optionalConsultTime;
        // ne asiguram ca user-ul chiar daca introduce o ora care nu este valida, programul nu va arunca erori
        while (true) {
            inputOutputService.displayAvailableTimeForConsult(availableForScheduling);
            String consultTimeAsString = inputOutputService.readYourAnswer();
            optionalConsultTime = consultTime(consultTimeAsString);
            if (optionalConsultTime.isPresent()) {
                break;
            } else{
                inputOutputService.displayErrorMessage("Input");
            }
        }
        return optionalConsultTime;
    }

    private Optional<LocalTime> consultTime(String consulTimeAsString){
        try{
            DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("H:mm");
            LocalTime consultTime = LocalTime.parse(consulTimeAsString, dateTimeFormatter);
            return Optional.of(consultTime);
        }catch (DateTimeParseException e) {
            return Optional.empty();
        }
    }

    private List<LocalTime> getAvailableForSchedulingHours(WorkDaySchedule workDaySchedule, List<LocalTime> scheduledConsulationHours) {
        List<LocalTime> availableForScheduling = new ArrayList<>(); // lista e goala
        LocalTime candidateTIme = workDaySchedule.getStartTime();
        while(candidateTIme.isBefore(workDaySchedule.getEndTime())){ // cat inca nu am ajuns la end time
            if(!scheduledConsulationHours.contains(candidateTIme)){// daca candidate time nu e dea booked, putem inca
                availableForScheduling.add(candidateTIme);
            }
            candidateTIme = candidateTIme.plusHours(1);
        }
        return availableForScheduling;
    }

    private List<LocalTime> getScheduledConsultationHours(Vet vet, LocalDate finalLocalDateForConsult) {
        // am folost un stream pentru a stoca intr-o lista de LocalTime toate orele deja rezervate pt consultatii din data selectata de user
        return vet.getConsultList().stream()
            .filter(consult -> consult.getDateTime().toLocalDate().equals(finalLocalDateForConsult))
            .map(Consult::getTime)
            .collect(Collectors.toList());
    }

    private Optional<WorkDaySchedule> getWorkDaySchedule(Vet vet, LocalDate localDateForConsult) {
        Optional<WorkDaySchedule> scheduleFor = vet.getScheduleFor(localDateForConsult.getDayOfWeek());
        if(!scheduleFor.isPresent()){
            inputOutputService.vetErrorMessage();
        }
        return scheduleFor;
    }

    private Optional<LocalDate> getConsultDate() {
        String dateOfConsultAsString = inputOutputService.readDate("date for consult");
        return utilityService.parseDate(dateOfConsultAsString);
    }

    private Vet getVet() {
        List<Vet> allVets = vetDao.findAll();
        inputOutputService.displayVetsAppointmentMessage(allVets);
        while(true) {
            String vetIndexAsString = inputOutputService.readYourAnswer();
            Optional<Integer> vetIndex = parseVetIndex(vetIndexAsString);
            if(vetIndex.isPresent()) {
                int vetIndx = vetIndex.get();
                // facem aceasta conditie pentru a ne asigura ca aplicatia nu se va opri cand input-ul
                // o sa fie de exemplu (-1, -2 sau vetIndex +1, +2 etc)
                if (isIndexValid(allVets, vetIndx)) {
                    // aici ne asiguram ca luam vet-ul de pe pozitia care trebuie
                    // deoarece daca nu am face vetIndx -1, noi putem selecta vet-ul cu indexul 10
                    // dar el se afla pe pozitia 9 iar atunci o sa avem un IndexOutOfBound
                    return allVets.get(vetIndx-1);
                }
            }
            inputOutputService.displayErrorMessage("Input ");
        }
    }

    private boolean isIndexValid(List<Vet> allVets, int vetIndx) {
        return vetIndx > 0 && vetIndx <= allVets.size();
    }

    private Optional<Integer> parseVetIndex(String vetIndexAsString) {
        try{
            int vetIndex = Integer.parseInt(vetIndexAsString);
            return Optional.of(vetIndex);
        }catch (NumberFormatException e){
            return Optional.empty();
        }
    }

    private void addPet() { // adaugam pet-ul
        String petFirstName = inputOutputService.readPet("first name");
        LocalDate petBirthDate = getPetBirthDate();
        PetType optionalPetType = getPetType();
        Boolean optionalPetIsVaccinated = getPetIsVaccinated();
        String petOwnerName = inputOutputService.readPetOwnerName("owner name");
        Pet pet = new Pet(petFirstName, petBirthDate, optionalPetType, petOwnerName, optionalPetIsVaccinated);
        petDao.save(pet);
    }

    private Boolean getPetIsVaccinated() {
        while(true) {
            String petIsVacinatedAsString = inputOutputService.readPetIsVaccinated("pet");
            Optional<Boolean> optionalPetIsVaccinated = parseIsVaccinated(petIsVacinatedAsString);
            if(optionalPetIsVaccinated.isPresent()){
                return optionalPetIsVaccinated.get();
            }
            inputOutputService.displayErrorMessage("Input");
        }
    }

    private PetType getPetType() {
        while(true) {
            String petTypeAsString = inputOutputService.readPet( "pet type");
            Optional<PetType> optionalPetType = parsePetType(petTypeAsString);
            if(optionalPetType.isPresent()){
                return optionalPetType.get();
            }
            inputOutputService.displayErrorMessage("Pet Type");
        }
    }

    private LocalDate getPetBirthDate() {
        while(true) {
            String petBirthDateAsString = inputOutputService.readDate("pet birth date");
            Optional<LocalDate> petBirthDate = utilityService.parseDate(petBirthDateAsString);
            if(petBirthDate.isPresent()){
                return petBirthDate.get();
            }
            inputOutputService.displayErrorMessage("Date");
        }
    }

    // covertim String-ul intr-un boolean
    private Optional<Boolean> parseIsVaccinated(String petIsVacinatedAsString) {
        try{
            Boolean result;
            if (petIsVacinatedAsString.equalsIgnoreCase("Yes")) {
                result = true;
            }else if(petIsVacinatedAsString.equalsIgnoreCase("No")){
                result = false;
            }else{
                result = null;
            }
            return Optional.of(result);
        } catch (NullPointerException e) {
            return Optional.empty();
        }
    }

    // convertim String-ul intr-un PetType
    // folosim Optional deoarece exista posibilitatea ca user-ul sa introduca un petType care nu este valid
    // Atunci petType-ul invalid nu va fi intorudus (se va returna o cutie goala)
    private Optional<PetType> parsePetType(String petTypeAsString) {
        try{
        PetType petType = PetType.valueOf(petTypeAsString.toUpperCase());
        return Optional.of(petType);
        }catch (IllegalArgumentException e){
            return Optional.empty();
        }
    }

    // adaugam Vet-ul (firstName, lastName etc...)
    private void addVet() {
        String vetFirstName = inputOutputService.readVet("FIrst Name");
        String vetLastName = inputOutputService.readVet("Last Name");
        String vetAddress = inputOutputService.readVet("address");
        String vetSpecialities = inputOutputService.readVet("specialities separated by space");
        Vet vet = new Vet(vetFirstName, vetLastName, vetAddress, vetSpecialities);
        vetDao.save(vet);// cu instanta creata am putut accesa metoda save care adauga in baza de date un vet nou
    }
}
