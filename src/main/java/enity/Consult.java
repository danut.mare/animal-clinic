package enity;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.time.LocalTime;

@Entity
public class Consult {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    // id-ul o sa fie generat automat deoarece daca intr-un table o sa fie introoduse 10 intrari iar intrarea nr. 5 se va sterge,
    // ne vom asigura ca nu vor exista id "stirbe" (adica 4, 6, 7, 8 etc)
    private Integer id;

    @ManyToOne // folosim ManyToOne deoarece pot exista mai multe coonsulturi pentru un Veterinar
    @JoinColumn // ii spunem Hibernat-ului sa creeze o coloana in care sa stocheze info despre VET
    private Vet vet;

    @ManyToOne
    @JoinColumn
    private Pet pet;
    private LocalDateTime dateTime;
    private String description;

    public Consult(){

    }

    public Consult(Vet vet, Pet pet, LocalDateTime dateTime, String description) {
        this.vet = vet;
        this.pet = pet;
        this.dateTime = dateTime;
        this.description = description;
    }

    public Vet getVet() {
        return vet;
    }

    public void setVet(Vet vet) {
        this.vet = vet;
    }

    public Pet getPet() {
        return pet;
    }

    public void setPet(Pet pet) {
        this.pet = pet;
    }

    public LocalDateTime getDateTime() {
        return dateTime;
    }

    public void setDateTime(LocalDateTime dateTime) {
        this.dateTime = dateTime;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public LocalTime getTime() {
        return dateTime.toLocalTime();
    }

}
