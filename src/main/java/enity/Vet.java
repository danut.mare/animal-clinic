package enity;

import javax.persistence.*;
import java.time.DayOfWeek;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


@Entity
public class Vet {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    // id-ul o sa fie generat automat deoarece daca intr-un table o sa fie introoduse 10 intrari iar intrarea nr. 5 se va sterge,
    // ne vom asigura ca nu vor exista id "stirbe" (adica 4, 6, 7, 8 etc)
    private Integer id;

    private String firstName;
    private String lastName;
    private String address;

    @ElementCollection(targetClass = PetType.class) // ii spunem ca vrem sa punem mai multe elemente de tipul pet type
    @Enumerated(EnumType.STRING) // punem valorle in format STRING
    @CollectionTable(name = "vet_speciality") // aici am facut un table nou cu numele de  vet_speiality
    private List<PetType> speciality;// cream o lista de PetType deoarece un Vet poate avea mai multe specializari(CAT, DOG etc..)

    @OneToMany(mappedBy = "vet", cascade = CascadeType.ALL)
    // pentru ca Vet-ul sa isi poata vedea proprile consulturi,
    // trebuie sa folosim mappedBy si sa ii atribuim numele field-ului din Consult care face referire catre Veterinar
    private List<Consult> consultList;
    @OneToMany(mappedBy = "vet", cascade = CascadeType.ALL)
    private List<WorkDaySchedule> workDaySchedules;

    public Vet(){}

    public Vet(String firstName, String lastName, String address, String specialities) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.address = address;
        this.speciality = convertToEnumList(specialities);
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public List<PetType> getSpeciality() {
        return speciality;
    }

    public void setSpeciality(List<PetType> speciality) {
        this.speciality = speciality;
    }

    public List<Consult> getConsultList() {
        return consultList;
    }

    public void setConsultList(List<Consult> consultList) {
        this.consultList = consultList;
    }

    public List<WorkDaySchedule> getWorkDaySchedules() {
        return workDaySchedules;
    }

    public void setWorkDaySchedules(List<WorkDaySchedule> workDaySchedules) {
        this.workDaySchedules = workDaySchedules;
    }

    private List<PetType> convertToEnumList(String specialities) {
        String[] specialitiesArray = specialities.split(" ");// ca delimitator folosim un spatiu " "
        List<PetType> result = new ArrayList<>();
        for(String speciality : specialitiesArray) {
            try { // introducem un bloc de try - catch pentru cazurile in care user-ul introduce o specialitate invalida
                PetType petType = PetType.valueOf(speciality.toUpperCase());// aici pentru fiecare element speciality il convertim ca un PetType
                result.add(petType);// adaugam fiecare petType in arratList-ul result
            }catch (IllegalArgumentException e){
                System.out.println(speciality + " is not a valid Speciality");
            }
        }
        return result;
    }

    public Optional<WorkDaySchedule> getScheduleFor(DayOfWeek day) {
        for(WorkDaySchedule workDaySchedule : workDaySchedules){
            if(workDaySchedule.getDayOfWeek().equals(day)){
                return Optional.of(workDaySchedule);
            }
        }
        return Optional.empty();
    }

    public void addSchedule(WorkDaySchedule workDaySchedule) {
        workDaySchedules.add(workDaySchedule);
    }
}
