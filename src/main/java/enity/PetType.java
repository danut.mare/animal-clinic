package enity;
// Initialiazm un ENUM PetType stabilind specialitatile clinicii
public enum PetType {
    DOG,
    CAT,
    HORSE,
    PARROT,
    HAMSTER
}
