package enity;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

@Entity
public class Pet {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    // id-ul o sa fie generat automat deoarece daca intr-un table o sa fie introoduse 10 intrari iar intrarea nr. 5 se va sterge,
    // ne vom asigura ca nu vor exista id "stirbe" (adica 4, 6, 7, 8 etc)
    private Integer id;

    private String name;
    private LocalDate birthDate;
    private PetType type;
    private String ownerName;
    private boolean isVaccninated;

    @OneToMany(mappedBy = "pet") // aceasi chestie o facem si aici (Ca la VET)
    private List<Consult> consultList;

    public String name(){
        return name;
    }

    public void setName(String name){
        this.name = name;
    }

    public LocalDate getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(LocalDate birthDate) {
        this.birthDate = birthDate;
    }

    public PetType getType() {
        return type;
    }

    public void setType(PetType type) {
        this.type = type;
    }

    public String getOwnerName() {
        return ownerName;
    }

    public void setOwnerName(String ownerName) {
        this.ownerName = ownerName;
    }

    public boolean isVaccninated() {
        return isVaccninated;
    }

    public void setVaccninated(boolean vaccninated) {
        isVaccninated = vaccninated;
    }

    public Pet(){}

    public Pet(String name, LocalDate birthDate, PetType type, String ownerName, boolean isVaccninated) {
        this.name = name;
        this.birthDate = birthDate;
        this.type = type;
        this.ownerName = ownerName;
        this.isVaccninated = isVaccninated;
    }
}
