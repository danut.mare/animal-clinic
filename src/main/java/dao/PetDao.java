package dao;

import enity.Pet;
import enity.Vet;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import org.hibernate.query.QueryParameter;


import java.util.Optional;

// PetDao o sa inlocuiasca acel "X" din GenericDao cu un Pet
public class PetDao extends GenericDao<Pet>{
    // in aceasta metoda dorim sa extragem din baza de date Pet-ul intorudus de utilizator cat si numele stapanului
    // asa pet-ul va avea un identificator prin numle stapanunlui
    public Optional<Pet> findByOwnerNameAndPetName(String ownerName, String petName) {
        Session session = sessionFactory.openSession();
        Query<Pet> petQuery = session.createQuery("select p from Pet p where p.name = :petName and p.ownerName = :ownerName");
        petQuery.setParameter("petName", petName);
        petQuery.setParameter("ownerName", ownerName);
        return petQuery.uniqueResultOptional(); // o sa arunce exceptie daca avem 2 pets cu acelasi nume si acelasi ownerName
    }
}
