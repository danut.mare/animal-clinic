package dao;

import enity.Vet;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import java.util.List;

// VetDao o sa inlocuiasca acel "X" din GenericDao cu un Vet
public class VetDao extends GenericDao<Vet> {

    // afisam toti veterinarii disponibili pe care ii poate alege user-ul
    public List<Vet> findAll() {
        Session session = sessionFactory.openSession();
        Query <Vet> vetQuery = session.createQuery("from Vet");
        return vetQuery.list();
    }
}
