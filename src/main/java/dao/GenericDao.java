package dao;

import enity.Pet;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import util.HibernateUtil;
// Data Access Object
// Folosim aceast Pattern pentru a ne structura lucrul cu baza de date (scriere in baza de date, citire etc..)
//acesta este si logica de inserare in baza de date
// iar cu ajutorul Genric Type-ului pe care l-am initializat cu "X"(X-ul acesta tine locul unei clase (Pet, Vet... etc)
// metoda de save cu care salvam input-uile in baza de date nu va fi nevoie sa o scriem in toate clasele DAO
//ceea ce este okay deoarece ar fi foarte mult cod duplicat
public class GenericDao<X> {
    protected SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
    public void save(X x) {
        Session session = sessionFactory.openSession();//deschidem sessiunea si o stocam intr-o variabila
        Transaction transaction = session.beginTransaction();// pornim tranzactia si o stocam intr-o variabila
        session.save(x); // salvam obiectul
        transaction.commit();// dam commit la tranzactie
        session.close();// inchidem sesiunea
    }

    //aplicam un merge deoarece dorim sa introducem date noi
    // aferente unui user care deja exista
    public void merge(X x) {
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        session.merge(x);
        transaction.commit();
        session.close();

    }

}
