
package util;


import enity.Consult;
import enity.Pet;
import enity.Vet;
import enity.WorkDaySchedule;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.cfg.Environment;
import org.hibernate.service.ServiceRegistry;

import java.util.Properties;

public class HibernateUtil {
    private static SessionFactory sessionFactory = null;

    // folosim DesignPattern-ul Singletone deoarece dorim sa obtinem o singura conexiunea cu baza de date
    // nu dorim sa obtinem mai multe obiecte de tipul sessionFactory
    public static SessionFactory getSessionFactory(){
        if(sessionFactory == null){
            initSessionFactory();
        }
        return sessionFactory;
    }


    private static void initSessionFactory() {
        /*START BOILER PLATE CODE */
        Configuration configuration = new Configuration();
        Properties settings = new Properties();
        settings.put(Environment.DRIVER, "com.mysql.cj.jdbc.Driver");
        settings.put(Environment.URL, "jdbc:mysql://localhost:3306/animal_clinic?useSSL=false");
        settings.put(Environment.USER, "root");
        settings.put(Environment.PASS, "root");
        settings.put(Environment.DIALECT, "org.hibernate.dialect.MySQL55Dialect");
        settings.put(Environment.SHOW_SQL, "true");
        settings.put(Environment.CURRENT_SESSION_CONTEXT_CLASS, "thread");
        settings.put(Environment.HBM2DDL_AUTO, "update");

        // adaugam clasele pe care Hibernate-ul o sa le declare ca entitati
        configuration.addAnnotatedClass(Consult.class);
        configuration.addAnnotatedClass(Pet.class);
        configuration.addAnnotatedClass(Vet.class);
        configuration.addAnnotatedClass(WorkDaySchedule.class);
        configuration.addAnnotatedClass(Consult.class);
        configuration.setProperties(settings);

        ServiceRegistry serviceRegistry = new StandardServiceRegistryBuilder()
                .applySettings(configuration.getProperties()).build();
        sessionFactory = configuration.buildSessionFactory(serviceRegistry);
        /*END BOILER PLATE CODE*/
    }

}
