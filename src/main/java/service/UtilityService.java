package service;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.Optional;

// In aceasta clasa o sa stocam toate metodele de parsare
public class UtilityService {
    // convertim String-ul intr-un LocalDate
    public Optional<LocalDate> parseDate(String dateAsString) {
        try {
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
            LocalDate result = LocalDate.parse(dateAsString, formatter);
            return Optional.of(result);
        }catch (DateTimeParseException e){
            return Optional.empty();
        }
    }

    public LocalTime parseTime(String consultTimeAsString) {
        LocalTime consultTime;
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("H:mm");
        consultTime = LocalTime.parse(consultTimeAsString, dateTimeFormatter);
        return consultTime;
    }




}
