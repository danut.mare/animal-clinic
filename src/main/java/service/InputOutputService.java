package service;

import enity.Vet;

import java.time.DayOfWeek;
import java.time.LocalTime;
import java.util.List;
import java.util.Scanner;


// in aceasta clasa avem toate metodele de afisare
public class InputOutputService {
    // instantiem scanner-ul la nivel de clasa pentru a fi accesibil de toate metodele
    private Scanner scanner = new Scanner(System.in);

    public void displayMenu() {
        System.out.println("Welcome to our Clinic. Please choose one of the following options: ");
        System.out.println("1 - Add Vet");
        System.out.println("2 - Add Pet");
        System.out.println("3 - Schedule a consult");
        System.out.println("4 - Add work interval for vet");
        System.out.println("X - Exit");
    }

    public String getUserInput() {
        System.out.print("Your answer: ");
        return scanner.nextLine();
    }

    // am creat aceasta metoda cu scopul de a ne minimaliza codul pe care il scriem
    // cu acest readVet nu mai este nevoie sa folosim acelasi mesaj duplicat pentru fiecare caracteristica a vet-ului
    public String readVet(String detail) {
        return read("Vet", detail);
    }
    public String readPetOwnerName(String detail){
        return read("Pet", detail);
    }

    public String readPet(String detail) {
        return read("Pet", detail);
    }
    // ne folosim de aceata metoda generica pentru a simplifica putin codul si de a renunta la orice cod duplicat
    public String read(String element, String detail) {
        System.out.print("Please enter a " + element + " " + detail + ": ");
        return scanner.nextLine();
    }

    public String read(String detail) {
        System.out.print("Please enter " + detail + ": ");
        return scanner.nextLine();
    }

    public String readDate(String detail){
        System.out.print("OPlease enter a " + detail + " in the DD-MM-YYYY format:");
        return scanner.nextLine();
    }

    public String readPetIsVaccinated(String detail){
        System.out.print("Is your pet " + detail + "? Answer with Yes/No: ");
        return scanner.nextLine();
    }

    public void displayErrorMessage(String element){

        System.out.println(element + " not valid!");
    }

    // aceasta metoda va apela si veterinarii cat si mesajul de mai jos
    public void displayVetsAppointmentMessage(List<Vet> allVets) {
        System.out.println("Which vet would you like to create an appoinment with?");
        displayVets(allVets);
    }

    // am creat aceasta metoda deoarece cine are nevoie sa afiseze doar veterinarii o sa o apeleze pe ea
    public void displayVets(List<Vet> allVets) {
        for (int index = 0; index < allVets.size(); index++) {
            System.out.println((index + 1) + " - " + allVets.get(index).getLastName());
        }
    }

    public String readYourAnswer() {
        System.out.print("Your answer: ");
        return scanner.nextLine();
    }

    public String readDescription() {
        System.out.println("What's the reson of the consult?");
        return scanner.nextLine();
    }

    public void witchVetQuestion() {
        System.out.println("Which vet would you like to add a work interval for?");
    }

    public String getStartingHour(DayOfWeek day) {
        System.out.print("Please insert start hour for " + day.name() + ": ");
        return scanner.nextLine();
    }

    public String getEndHours(DayOfWeek day) {
        System.out.print("Please insert end hour for " + day.name() + ": ");
        return scanner.nextLine();
    }

    public void consultScheduled() {
        System.out.println("Your consult has been scheduled!");
    }

    public void noPetsError() {
        System.out.println("Sorry, there is not pet in our database with these details");
    }

    public void allBookedMessage() {
        System.out.println("All booked for this date. Try again with a different date");
    }

    public void vetErrorMessage() {
        System.out.println("Vet is not available on this date");
    }

    public void displayAvailableTimeForConsult(List<LocalTime> availableForScheduling) {
        System.out.println("Please pick one of the following time for the consult: ");
        for (LocalTime x : availableForScheduling) {
            System.out.println(x);
        }
    }
}
